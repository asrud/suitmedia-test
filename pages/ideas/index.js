import Head from "next/head";
import React from "react";
import Banner from "../../components/Organisms/Banner";
import Blog from "../../components/Organisms/Blog";
import Layout from "../../components/Templates/Layout";

function Ideas() {
  return (
    <>
      <Head>
        <title>Suitmedia - Digital Agency</title>
        <meta
          name="description"
          content="Digital solution for your bussiness"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Layout>
        <Banner />
        <Blog />
      </Layout>
    </>
  );
}

export default Ideas;
