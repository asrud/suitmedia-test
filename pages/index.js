import Head from "next/head";
import Layout from "../components/Templates/Layout";

export default function Home() {
  return (
    <>
      <Head>
        <title>Suitmedia - Digital Agency</title>
        <meta
          name="description"
          content="Digital solution for your bussiness"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Layout>
        <div className="vh-100 vw-100 d-flex justify-content-center align-items-center">
          <h1>Pelase Enter to Ideas Page</h1>
        </div>
      </Layout>
    </>
  );
}
