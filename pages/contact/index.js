import React from "react";
import Layout from "../../components/Templates/Layout";

function Contact() {
  return (
    <>
      <Layout>
        <div className="vh-100 vw-100 d-flex justify-content-center align-items-center">
          <span>Contact</span>
        </div>
      </Layout>
    </>
  );
}

export default Contact;
