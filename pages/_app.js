import { useEffect } from "react";
import "../styles/globals.scss";

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    if (typeof document !== "undefined") {
      import("bootstrap/dist/js/bootstrap");
    }
  }, []);

  return <Component {...pageProps} />;
}

export default MyApp;
