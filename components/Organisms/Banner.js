import Image from "next/image";
import React, { useEffect } from "react";

function Banner() {
  return (
    <>
      <div className="row mb-4">
        <div className="col">
          <div className="d-flex flex-column justify-content-center align-items-center position-relative text-white background-banner">
            <h1 className="mb-0">Ideas</h1>
            <span>Where all our great things begin</span>
            <div className="gradient-dark" />
          </div>
        </div>
      </div>
    </>
  );
}

export default Banner;
