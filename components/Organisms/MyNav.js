import React, { useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";

function MyNav() {
  const router = useRouter();

  useEffect(() => {
    let prevScrollpos = window.pageYOffset;
    window.onscroll = function () {
      let currentScrollPos = window.pageYOffset;
      if (prevScrollpos > currentScrollPos) {
        document.getElementById("navbar").style.opacity = "1";
        document.getElementById("navbar").style.transition =
          "all 0.7s ease 0.5s";
      } else {
        document.getElementById("navbar").style.opacity = "0";
      }
      prevScrollpos = currentScrollPos;
    };
  }, []);

  const navList = [
    {
      id: 1,
      path: "/work",
      name: "Work",
    },
    {
      id: 2,
      path: "/about",
      name: "About",
    },
    {
      id: 3,
      path: "/services",
      name: "Services",
    },
    {
      id: 4,
      path: "/ideas",
      name: "Ideas",
    },
    {
      id: 5,
      path: "/careers",
      name: "Careers",
    },
    {
      id: 6,
      path: "/contact",
      name: "Contact",
    },
  ];

  return (
    <nav className="navbar navbar-expand-lg fixed-top bg-primary" id="navbar">
      <div className="container">
        <Link href="/" legacyBehavior>
          <a className="navbar-brand">
            <Image
              src="/suitmedia.png"
              width={120}
              height={50}
              alt="suitmedia-logo"
            />
          </a>
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
            {navList.map((item) => (
              <li className="nav-item" key={item.id}>
                <Link
                  href={item.path}
                  className={
                    router.pathname === item.path
                      ? "active nav-link"
                      : "nav-link"
                  }
                  aria-current="page"
                >
                  {item.name}
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default MyNav;
