import React, { useState } from "react";
import BasicPagination from "../Molecules/BasicPagination";
import BlogCard from "../Molecules/BlogCard";

function Blog() {
  const [currentPage, setCurrentPage] = useState(1);

  const contentList = [
    {
      id: 1,
    },
    {
      id: 2,
    },
    {
      id: 3,
    },
    {
      id: 4,
    },
    {
      id: 5,
    },
    {
      id: 6,
    },
    {
      id: 7,
    },
    {
      id: 8,
    },
    {
      id: 9,
    },
    {
      id: 10,
    },
  ];

  return (
    <>
      <div className="container mt-5" style={{ height: "1000px" }}>
        <div className="d-flex flex-column flex-md-row align-items-center justify-content-between">
          <div className="mb-2 mb-md-0">
            <span>Showing 1-10 of 100</span>
          </div>
          <div className="d-flex">
            <div className="d-flex align-items-center me-4">
              <span>Show per page :</span>
              <div className="ms-2">
                <select
                  className="form-select rounded-form"
                  aria-label="Default select example"
                >
                  <option selected>10</option>
                  <option value="1">20</option>
                  <option value="2">50</option>
                </select>
              </div>
            </div>
            <div className="d-flex align-items-center">
              <span>Sort by: </span>
              <div className="ms-2">
                <select
                  className="form-select rounded-form"
                  aria-label="Default select example"
                >
                  <option selected>Newest</option>
                  <option value="1">Oldest</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div className="row g-4 mt-2">
          {contentList.map((item) => (
            <div className="col-sm-6 col-md-4 col-lg-3 col-xxl-2" key={item.id}>
              <BlogCard />
            </div>
          ))}
        </div>
        <div className="d-flex justify-content-center my-5">
          <BasicPagination
            pageCount={10}
            onPageChange={() => setCurrentPage(currentPage + 1)}
          />
        </div>
      </div>
    </>
  );
}

export default Blog;
