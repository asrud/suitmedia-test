import React from "react";
import MyNav from "../Organisms/MyNav";

function Layout({ children }) {
  return (
    <>
      <MyNav />
      <section>{children}</section>
    </>
  );
}

export default Layout;
