import React from "react";
import ReactPaginate from "react-paginate";
import { BsChevronLeft, BsChevronRight } from "react-icons/bs";

function BasicPagination({
  pageCount = 0,
  pageRangeDisplayed = 5,
  marginPagesDisplayed = 2,
  onPageChange,
  selectedPageRel = 0,
  className = "",
  forcePage,
}) {
  return (
    <ReactPaginate
      forcePage={forcePage}
      previousLabel={<BsChevronLeft />}
      nextLabel={<BsChevronRight />}
      breakLabel="..."
      breakClassName="break-me"
      selectedPageRel={selectedPageRel}
      pageCount={pageCount}
      marginPagesDisplayed={marginPagesDisplayed}
      pageRangeDisplayed={pageRangeDisplayed}
      onPageChange={onPageChange}
      containerClassName={`basic-pagination ${className}`}
      subContainerClassName="pages pagination"
      activeClassName="active"
    />
  );
}

export default BasicPagination;
