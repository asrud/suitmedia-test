import React from "react";
import Skeleton from "react-loading-skeleton";

function BlogCard() {
  return (
    <>
      <div className="card shadow w-100">
        <img src="/img/blog1.png" className="card-img-top" alt="..." />
        {/* <Skeleton height="100px" width="100%" /> */}
        <div className="card-body">
          <span className="card-title text-muted" style={{ fontSize: ".8rem" }}>
            5 SEPTEMBER 2022
          </span>
          <p className="card-text ellipsis">
            Kenali Tingkatan Influencers berdasarkan Jumlah Followers
          </p>
        </div>
      </div>
    </>
  );
}

export default BlogCard;
